package prob2;

public class Rectangle implements Polygon {
    private double length, width;

    public Rectangle(double length, double width) {
        this.length = length;
        this.width = width;
    }

    @Override
    public double[] getSides() {
        double[] sides = {2*length,2*width};

        return sides;
    }
}

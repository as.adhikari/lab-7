package prob2;

public class Ecllipse implements ClosedCurve {
    private double a, E;

    Ecllipse(double length, double e) {
        a = length;
        E = e;
    }

    @Override
    public double computePerimeter() {
        return 4*a*E;
    }
}

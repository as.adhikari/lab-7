package prob2;

public class EquilateralTriangle implements Polygon{
    double length;

    EquilateralTriangle(double length){
        this.length = length;
    }

    @Override
    public double[] getSides() {
        double[] sides = {3*length};

        return sides;
    }
}

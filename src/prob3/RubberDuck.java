package prob3;

public class RubberDuck implements Duck {

    @Override
    public void fly() {
        System.out.println("Cannot Fly");
    }

    @Override
    public void quack() {
        System.out.println("Squeaking");
    }
}

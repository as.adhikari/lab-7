package prob3;

public class DecoyDuck implements Duck{

    @Override
    public void fly() {
        System.out.println("Cannot Fly");
    }

    @Override
    public void quack() {
        System.out.println("Cannot quack");
    }
}

package prob3;

public interface Duck {

    default void swim() {
        System.out.println("Swimming");
    }

    default void fly() {
        System.out.println("Flying");
    }

    default void quack() {
        System.out.println("Quacking");
    }

    default void display() {
        System.out.println("displaying");
    }
}
